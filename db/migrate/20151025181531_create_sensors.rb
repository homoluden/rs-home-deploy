class CreateSensors < ActiveRecord::Migration
  def change
    create_table :sensors do |t|
      t.string :uid
      t.string :label
      t.integer :type

      t.timestamps null: false
    end
  end
end
