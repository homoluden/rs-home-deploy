class ChangeTypeToSensorType < ActiveRecord::Migration
  def change
    rename_column :sensors, :type, :sensor_type
  end
end
