class AddUidToRecords < ActiveRecord::Migration
  def change
    add_column :records, :uid, :string
  end
end
