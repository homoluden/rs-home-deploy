json.array!(@sensors) do |sensor|
  json.extract! sensor, :id, :uid, :label, :sensor_type
  json.url sensor_url(sensor, format: :json)
end
