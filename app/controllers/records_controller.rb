class RecordsController < ApplicationController
  before_action :append, only: [:post]
  protect_from_forgery with: :null_session, only: Proc.new { |c| c.request.format.json? }

  def append
    uid = params[:uid]
    txt = params[:txt]

    s = Sensor.where(:uid => "0672FF55-51538850-87123432").take

    if s
      s.records.create(uid: uid, message: txt, time: Time.now)
      render text: "OK:#{uid}<=#{txt}!"
    else
      render text: "ERR:#{uid}=>nil"
    end
  end

  def tail
    sensor_id = params[:sensor_id].to_i
    sensor = (sensor_id && sensor_id != 0) ? Sensor.find(sensor_id) : nil

    unless sensor
      render json: {err: "Sensor not found!", status: :err}
    else
      tail = params[:tail].to_i
      records = sensor.records[(tail == 0) ? -10 : -tail, sensor.records.length]
      render json: {records: records, status: :ok}
    end
  end

end
