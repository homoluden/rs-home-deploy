class Sensor < ActiveRecord::Base
  enum sensor_type: [:pir, :dht, :flood, :combo]
  SENSOR_TYPES =
    [
       {:value => :pir, :label => "PIR"},
       {:value => :dht, :label => "DHT"},
       {:value => :flood, :label => "FLOOD"},
       {:value => :combo, :label => "COMBO"}
    ]

  has_many :records, dependent: :destroy

  def records_tail
    records[-10,records.length]
  end

end
